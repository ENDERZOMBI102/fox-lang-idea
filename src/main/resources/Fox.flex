package com.enderzombi102.foxlang.idea.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static com.enderzombi102.foxlang.idea.psi.FoxElementTypes.*;

%%

%public
%class FoxLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+

T_DOC_COMMENT=("/"\*\*(.|\n)*\*"/")
T_BLOCK_COMMENT=("/"\*(.|\n)*\*"/")
T_LINE_COMMENT=("//".*)
T_WHITE_SPACE=[ \t\n\x0B\f\r]+
T_NEWLINE=\R+
T_IDENT=([a-zA-Z_$][a-zA-Z_$0-9]*)
T_INTEGER=((0x[0-9A-Fa-f]+)|[1-9][0-9]*|(0[0-7]+)|0|'.')
T_FLOAT=([0-9]+\.([e|E][+|\-]?)?[0-9]+)
T_STRING=\"([^\"\\]|\\.)*\"

%%
<YYINITIAL> {
  {WHITE_SPACE}          { return WHITE_SPACE; }

  "abstract"             { return T_ABSTRACT; }
  "as"                   { return T_AS; }
  "asm"                  { return T_ASM; }
  "pkg"                  { return T_PKG; }
  "den"                  { return T_DEN; }
  "type"                 { return T_TYPE; }
  "attribute"            { return T_ATTRIBUTE; }
  "break"                { return T_BREAK; }
  "by"                   { return T_BY; }
  "cast"                 { return T_CAST; }
  "class"                { return T_CLASS; }
  "comptime"             { return T_COMPTIME; }
  "const"                { return T_CONST; }
  "continue"             { return T_CONTINUE; }
  "do"                   { return T_DO; }
  "else"                 { return T_ELSE; }
  "enum"                 { return T_ENUM; }
  "export"               { return T_EXPORT; }
  "extern"               { return T_EXTERN; }
  "for"                  { return T_FOR; }
  "from"                 { return T_FROM; }
  "if"                   { return T_IF; }
  "import"               { return T_IMPORT; }
  "in"                   { return T_IN; }
  "interface"            { return T_INTERFACE; }
  "is"                   { return T_IS; }
  "like"                 { return T_LIKE; }
  "loop"                 { return T_LOOP; }
  "macro"                { return T_MACRO; }
  "memory"               { return T_MEMORY; }
  "mut"                  { return T_MUT; }
  "operator"             { return T_OPERATOR; }
  "override"             { return T_OVERRIDE; }
  "pub"                  { return T_PUB; }
  "reinterpret"          { return T_REINTERPRET; }
  "return"               { return T_RETURN; }
  "runtime"              { return T_RUNTIME; }
  "sealed"               { return T_SEALED; }
  "static"               { return T_STATIC; }
  "struct"               { return T_STRUCT; }
  "typealias"            { return T_TYPEALIAS; }
  "when"                 { return T_WHEN; }
  "while"                { return T_WHILE; }
  "yield"                { return T_YIELD; }
  "~"                    { return T_TILDE; }
  "!"                    { return T_NOT; }
  "!="                   { return T_NOT_EQ; }
  "||"                   { return T_COND_OR; }
  "=="                   { return T_EQ; }
  "="                    { return T_ASSIGN; }
  "&&"                   { return T_COND_AND; }
  "<"                    { return T_LESS; }
  "<="                   { return T_LESS_OR_EQUAL; }
  ">"                    { return T_GREATER; }
  ">="                   { return T_GREATER_OR_EQUAL; }
  "<=>"                  { return T_COMPARE; }
  "+"                    { return T_PLUS; }
  "+="                   { return T_PLUS_ASSIGN; }
  "-"                    { return T_MINUS; }
  "-="                   { return T_MINUS_ASSIGN; }
  "/"                    { return T_QUOTIENT; }
  "/="                   { return T_QUOTIENT_ASSIGN; }
  "*"                    { return T_MUL; }
  "*="                   { return T_MUL_ASSIGN; }
  "**"                   { return T_POW; }
  "**="                  { return T_POW_ASSIGN; }
  "%"                    { return T_REMAINDER; }
  "%="                   { return T_REMAINDER_ASSIGN; }
  "++"                   { return T_PLUS_PLUS; }
  "--"                   { return T_MINUS_MINUS; }
  "<-"                   { return T_SEND_CHANNEL; }
  "&"                    { return T_BIT_AND; }
  "&="                   { return T_BIT_AND_ASSIGN; }
  "^"                    { return T_BIT_XOR; }
  "^="                   { return T_BIT_XOR_ASSIGN; }
  "|"                    { return T_BIT_OR; }
  "|="                   { return T_BIT_OR_ASSIGN; }
  ">>"                   { return T_SHIFT_RIGHT; }
  ">>="                  { return T_SHIFT_RIGHT_ASSIGN; }
  "<<"                   { return T_SHIFT_LEFT; }
  "<<="                  { return T_SHIFT_LEFT_ASSIGN; }
  ">>>"                  { return T_SHIFT_SHIFT_RIGHT; }
  "{"                    { return T_LBRACE; }
  "}"                    { return T_RBRACE; }
  "["                    { return T_LBRACK; }
  "]"                    { return T_RBRACK; }
  "("                    { return T_LPAREN; }
  ")"                    { return T_RPAREN; }
  ","                    { return T_COMMA; }
  "."                    { return T_DOT; }
  ":"                    { return T_COLON; }
  "::"                   { return T_COLONCOLON; }
  "\""                   { return T_DQUOTE; }
  "->"                   { return T_ARROW_R; }
  "this"                 { return T_THIS; }
  "true"                 { return T_TRUE; }
  "false"                { return T_FALSE; }

  {T_DOC_COMMENT}        { return T_DOC_COMMENT; }
  {T_BLOCK_COMMENT}      { return T_BLOCK_COMMENT; }
  {T_LINE_COMMENT}       { return T_LINE_COMMENT; }
  {T_WHITE_SPACE}        { return T_WHITE_SPACE; }
  {T_NEWLINE}            { return T_NEWLINE; }
  {T_IDENT}              { return T_IDENT; }
  {T_INTEGER}            { return T_INTEGER; }
  {T_FLOAT}              { return T_FLOAT; }
  {T_STRING}             { return T_STRING; }

}

[^] { return BAD_CHARACTER; }
