package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.highlight.FoxSyntaxHighlighter
import com.intellij.openapi.diagnostic.thisLogger
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.options.colors.AttributesDescriptor
import com.intellij.openapi.options.colors.ColorDescriptor
import com.intellij.openapi.options.colors.ColorSettingsPage
import javax.swing.Icon

typealias Desc = AttributesDescriptor

class FoxColorSettingsPage : ColorSettingsPage {
	companion object {
		val DESCRIPTORS = arrayOf(
			Desc( "Dot", FoxSyntaxHighlighter.DOT ),
			Desc( "Keyword", FoxSyntaxHighlighter.KEYWORD ),
			Desc( "Type", FoxSyntaxHighlighter.TYPE ),
			Desc( "Literals//String", FoxSyntaxHighlighter.STRING ),
			Desc( "Literals//Number", FoxSyntaxHighlighter.NUMBER ),
			Desc( "Comments//Line comment", FoxSyntaxHighlighter.LINE_COMMENT ),
			Desc( "Comments//Block comment", FoxSyntaxHighlighter.BLOCK_COMMENT ),
			Desc( "Comments//Doc comment", FoxSyntaxHighlighter.DOC_COMMENT ),
			Desc( "Bad character", FoxSyntaxHighlighter.BAD_CHARACTER ),
		)
	}

	override fun getAttributeDescriptors(): Array<AttributesDescriptor> =
		DESCRIPTORS

	override fun getColorDescriptors(): Array<ColorDescriptor> =
		ColorDescriptor.EMPTY_ARRAY

	override fun getDisplayName(): String =
		"Fox"

	override fun getIcon(): Icon =
		FoxIcons.FILE

	override fun getHighlighter(): SyntaxHighlighter =
		FoxSyntaxHighlighter()

	override fun getDemoText(): String {
		val code = javaClass.getResourceAsStream("/colorSettingsDemoText.fox")?.readAllBytes()?.decodeToString()

		if ( code != null )
			return code

		thisLogger().warn( "Resource failed to read!" )
		return "// Failed to load demo text"
	}

	override fun getAdditionalHighlightingTagToDescriptorMap(): MutableMap<String, TextAttributesKey>? =
		null
}
