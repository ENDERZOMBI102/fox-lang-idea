package com.enderzombi102.foxlang.idea.inject

import com.enderzombi102.foxlang.idea.psi.FoxLiteral
import com.intellij.psi.PsiLanguageInjectionHost
import org.intellij.plugins.intelliLang.inject.AbstractLanguageInjectionSupport
import org.intellij.plugins.intelliLang.inject.config.BaseInjection
import org.jdom.Element


class FoxInjectionSupport : AbstractLanguageInjectionSupport() {
	override fun getId(): String =
		"fox"

	override fun getPatternClasses(): Array<Class<*>> =
		arrayOf( FoxPatterns::class.java )

	override fun isApplicableTo( host: PsiLanguageInjectionHost? ): Boolean =
		host is FoxLiteral && host.tString != null

	override fun createInjection( element: Element? ): BaseInjection =
		BaseInjection( "fox" )
}
