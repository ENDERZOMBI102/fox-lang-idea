package com.enderzombi102.foxlang.idea.inject

import com.intellij.lang.injection.MultiHostRegistrar
import com.intellij.lang.injection.general.Injection
import com.intellij.lang.injection.general.LanguageInjectionPerformer
import com.intellij.openapi.util.Trinity
import com.intellij.psi.ElementManipulators
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiLanguageInjectionHost
import org.intellij.plugins.intelliLang.inject.InjectedLanguage
import org.intellij.plugins.intelliLang.inject.InjectorUtils
import org.intellij.plugins.intelliLang.inject.config.BaseInjection

class FoxInjectionPerformer : LanguageInjectionPerformer {
	override fun isPrimary(): Boolean =
		true

	override fun performInjection( registrar: MultiHostRegistrar, injection: Injection, context: PsiElement ): Boolean {
		if ( context !is PsiLanguageInjectionHost || !context.isValidHost )
			return false

		val containingFile = context.getContainingFile()
		val injectedLanguage = InjectedLanguage.create(
			injection.injectedLanguageId,
			injection.prefix,
			injection.suffix,
			false
		)

		val injectionSupportId = injection.supportId
		val support = if ( injectionSupportId != null ) InjectorUtils.findInjectionSupport( injectionSupportId ) else null

		val language = injectedLanguage!!.language
		val manipulator = ElementManipulators.getManipulator( context )
		if ( language == null || manipulator == null )
			return injection is BaseInjection && InjectorUtils.registerInjectionSimple( context, injection, support, registrar )

		InjectorUtils.registerInjection(
			language,
			listOf( Trinity.create( context, injectedLanguage, manipulator.getRangeInElement( context ) ) ),
			containingFile,
			registrar
		)
		if ( support != null )
			InjectorUtils.registerSupport( support, false, context, language )
		return true
	}
}
