package com.enderzombi102.foxlang.idea.inject

import com.intellij.patterns.StandardPatterns
import com.intellij.patterns.StringPattern

class FoxPatterns {
	fun stringLiteral(): StringPattern =
		StandardPatterns.string()

}
