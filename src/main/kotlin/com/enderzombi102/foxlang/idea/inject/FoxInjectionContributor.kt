package com.enderzombi102.foxlang.idea.inject

import com.enderzombi102.foxlang.idea.psi.FoxLiteral
import com.intellij.lang.injection.general.Injection
import com.intellij.lang.injection.general.LanguageInjectionContributor
import com.intellij.psi.PsiElement
import org.intellij.plugins.intelliLang.inject.InjectorUtils

class FoxInjectionContributor : LanguageInjectionContributor {
	override fun getInjection( context: PsiElement ): Injection? {
		if ( context !is FoxLiteral || context.tString == null )
			return null

		return InjectorUtils.findCommentInjection( context, "fox", null )
	}
}
