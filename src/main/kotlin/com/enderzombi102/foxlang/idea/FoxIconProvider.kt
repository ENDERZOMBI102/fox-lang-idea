package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxFuncDecl
import com.intellij.ide.IconProvider
import com.intellij.psi.PsiElement
import javax.swing.Icon

class FoxIconProvider : IconProvider() {
	override fun getIcon( element: PsiElement, flags: Int ): Icon? = when ( element ) {
		is FoxFuncDecl -> FoxIcons.FILE
		else -> null
	}
}
