package com.enderzombi102.foxlang.idea.parser

import com.enderzombi102.foxlang.idea.FoxLanguage
import com.enderzombi102.foxlang.idea.lexer.FoxLexerAdapter
import com.enderzombi102.foxlang.idea.psi.FoxElementTypes
import com.enderzombi102.foxlang.idea.psi.FoxPsiFile
import com.enderzombi102.foxlang.idea.psi.FoxTokenSets
import com.intellij.lang.ASTNode
import com.intellij.lang.ParserDefinition
import com.intellij.lang.PsiParser
import com.intellij.lexer.Lexer
import com.intellij.openapi.project.Project
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IFileElementType
import com.intellij.psi.tree.TokenSet

class FoxParserDefinition : ParserDefinition {
	companion object { val FILE = IFileElementType( FoxLanguage ) }

	override fun createLexer( project: Project? ): Lexer =
		FoxLexerAdapter()

	override fun createParser( project: Project? ): PsiParser =
		_root_ide_package_.com.enderzombi102.foxlang.idea.parser.FoxParser()

	override fun getFileNodeType(): IFileElementType =
		FILE

	override fun getCommentTokens(): TokenSet =
		FoxTokenSets.COMMENTS

	override fun getStringLiteralElements(): TokenSet =
		FoxTokenSets.STRINGS

	override fun createElement( node: ASTNode? ): PsiElement =
		FoxElementTypes.Factory.createElement( node )

	override fun createFile( viewProvider: FileViewProvider ): PsiFile =
		FoxPsiFile( viewProvider )
}
