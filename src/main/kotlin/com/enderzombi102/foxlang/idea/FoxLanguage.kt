package com.enderzombi102.foxlang.idea

import com.intellij.lang.Language

object FoxLanguage : Language( "Fox" )
