package com.enderzombi102.foxlang.idea

import com.intellij.openapi.fileTypes.LanguageFileType
import javax.swing.Icon

object FoxFileType : LanguageFileType( FoxLanguage ) {
	override fun getName(): String =
		"Fox"

	override fun getDescription(): String =
		"Fox language file"

	override fun getDefaultExtension(): String =
		"fox"

	override fun getIcon(): Icon =
		FoxIcons.FILE
}
