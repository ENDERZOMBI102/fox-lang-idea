package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxElementTypes
import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.patterns.PlatformPatterns
import com.intellij.util.ProcessingContext


class FoxCompletionContributor : CompletionContributor() {
	init {
		extend(
			CompletionType.BASIC,
			PlatformPatterns.psiElement( FoxElementTypes.FUNC_DECL ),
			object : CompletionProvider<CompletionParameters>() {
				override fun addCompletions( params: CompletionParameters, ctx: ProcessingContext, result: CompletionResultSet ) {
					result.addElement( LookupElementBuilder.create( "Hello" ) )
				}
			}
		)
	}
}
