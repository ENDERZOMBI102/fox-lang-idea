package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxElementTypes.*
import com.intellij.lang.BracePair
import com.intellij.lang.PairedBraceMatcher
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IElementType

class FoxPairedBraceMatcher : PairedBraceMatcher {
	override fun getPairs(): Array<BracePair> =
		PAIRS

	override fun isPairedBracesAllowedBeforeType( lbraceType: IElementType, contextType: IElementType? ): Boolean =
		true

	override fun getCodeConstructStart( file: PsiFile?, openingBraceOffset: Int ): Int =
		openingBraceOffset

	companion object {
		@JvmStatic
		val PAIRS = arrayOf(
			BracePair( T_LBRACE, T_RBRACE , true  ),
			BracePair( T_LBRACK, T_RBRACK , false ),
			BracePair( T_LPAREN, T_RPAREN , false )
		)
	}
}
