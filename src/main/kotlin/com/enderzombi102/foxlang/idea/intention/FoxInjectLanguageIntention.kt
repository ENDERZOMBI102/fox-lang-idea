package com.enderzombi102.foxlang.idea.intention

import com.enderzombi102.foxlang.idea.psi.FoxLiteral
import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile

class FoxInjectLanguageIntention : IntentionAction {
	override fun startInWriteAction(): Boolean {
		TODO("Not yet implemented")
	}

	override fun getText(): String {
		TODO("Not yet implemented")
	}

	override fun getFamilyName(): String {
		TODO("Not yet implemented")
	}

	override fun isAvailable( project: Project, editor: Editor?, file: PsiFile? ): Boolean =
		file?.findElementAt( editor!!.caretModel.offset ) is FoxLiteral

	override fun invoke( project: Project, editor: Editor?, file: PsiFile? ) {

	}
}
