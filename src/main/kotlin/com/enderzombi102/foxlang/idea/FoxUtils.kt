package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxFuncDecl
import com.enderzombi102.foxlang.idea.psi.FoxPsiFile
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiManager
import com.intellij.psi.search.FileTypeIndex
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.util.PsiTreeUtil
import java.util.*


object FoxUtils {
	/**
	 * Searches the entire project for Simple language files with instances of the Simple property with the given key.
	 *
	 * @param project current project
	 * @param key     to check
	 * @return matching properties
	 */
	fun findFunctions( project: Project, key: String ): List<FoxFuncDecl> {
		val result = ArrayList<FoxFuncDecl>()
		val virtualFiles = FileTypeIndex.getFiles( FoxFileType, GlobalSearchScope.allScope( project ) )
		for ( virtualFile in virtualFiles ) {
			val foxFile = PsiManager.getInstance( project ).findFile( virtualFile!! ) as FoxPsiFile?
			if ( foxFile != null ) {
				PsiTreeUtil.getChildrenOfType( foxFile, FoxFuncDecl::class.java )?.apply {
					result.addAll( filter { key == it.name } )
				}
			}
		}
		return result
	}

	/**
	 * Searches the entire project for Simple language files with instances of the Simple property with the given key.
	 *
	 * @param project current project
	 * @return matching properties
	 */
	fun findFunctions( project: Project ): List<FoxFuncDecl> {
		val result = ArrayList<FoxFuncDecl>()
		val virtualFiles = FileTypeIndex.getFiles( FoxFileType, GlobalSearchScope.allScope( project ) )
		for ( virtualFile in virtualFiles ) {
			val foxFile = PsiManager.getInstance( project ).findFile( virtualFile!! ) as FoxPsiFile?
			if ( foxFile != null )
				PsiTreeUtil.getChildrenOfType( foxFile, FoxFuncDecl::class.java )?.apply( result::addAll )
		}
		return result
	}

	/**
	 * Attempts to collect any comment elements above the Simple key/value pair.
	 */
//	fun findDocumentationComment(property: SimpleProperty): String? {
//		val result: MutableList<String> = LinkedList()
//		var element: PsiElement = property.getPrevSibling()
//		while (element is PsiComment || element is PsiWhiteSpace) {
//			if (element is PsiComment) {
//				val commentText = element.getText().replaceFirst("[!# ]+".toRegex(), "")
//				result.add(commentText)
//			}
//			element = element.prevSibling
//		}
//		return StringUtil.join(Lists.reverse(result), "\n ")
//	}
}
