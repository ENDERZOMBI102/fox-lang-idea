package com.enderzombi102.foxlang.idea.lexer

import com.intellij.lexer.FlexAdapter

class FoxLexerAdapter : FlexAdapter(FoxLexer(null))
