package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.lexer.FoxLexerAdapter
import com.enderzombi102.foxlang.idea.psi.FoxFuncDecl
import com.enderzombi102.foxlang.idea.psi.FoxTokenSets
import com.intellij.lang.cacheBuilder.DefaultWordsScanner
import com.intellij.lang.cacheBuilder.WordsScanner
import com.intellij.lang.findUsages.FindUsagesProvider
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNamedElement

class FoxFindUsagesProvider : FindUsagesProvider {
	override fun getWordsScanner(): WordsScanner =
		DefaultWordsScanner( FoxLexerAdapter(), FoxTokenSets.IDENTIFIERS, FoxTokenSets.COMMENTS, FoxTokenSets.LITERALS )

	override fun canFindUsagesFor( psiElement: PsiElement ): Boolean =
		psiElement is PsiNamedElement

	override fun getHelpId( psiElement: PsiElement ): String? =
		null

	override fun getType( element: PsiElement ): String = when ( element ) {
		is FoxFuncDecl -> "fox function"
		else -> ""
	}

	override fun getDescriptiveName( element: PsiElement ): String = when ( element ) {
		is FoxFuncDecl -> element.identifier.text
		else -> ""
	}

	override fun getNodeText( element: PsiElement, useFullName: Boolean ): String = when ( element ) {
		is FoxFuncDecl -> element.text
		else -> ""
	}
}
