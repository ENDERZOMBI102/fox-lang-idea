package com.enderzombi102.foxlang.idea

import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiElementResolveResult
import com.intellij.psi.PsiReferenceBase
import com.intellij.psi.ResolveResult


class FoxPsiReference( element: PsiElement, textRange: TextRange ) : PsiReferenceBase<PsiElement>( element, textRange ) {
	private val key: String = element.text.substring( textRange.startOffset, textRange.endOffset )

	private fun multiResolve( incompleteCode: Boolean ): Array<ResolveResult> =
		FoxUtils.findFunctions( myElement.project, key )
			.map( ::PsiElementResolveResult )
			.toTypedArray()

	override fun resolve(): PsiElement? {
		val results = multiResolve( false )
		return if ( results.size == 1 ) results.first().element else null
	}

	override fun getVariants(): Array<Any> =
		FoxUtils.findFunctions( myElement.project )
			.map {
				LookupElementBuilder.create( it )
					.withIcon( FoxIcons.FILE )
					.withTypeText( it.containingFile.name )
			}
			.toTypedArray()
}
