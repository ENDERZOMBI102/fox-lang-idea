package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxFuncDecl
import com.intellij.lang.refactoring.RefactoringSupportProvider
import com.intellij.psi.PsiElement

class FoxRefactoringSupportProvider : RefactoringSupportProvider() {
	override fun isMemberInplaceRenameAvailable( element: PsiElement, context: PsiElement? ): Boolean = when ( element ) {
		is FoxFuncDecl -> true
		else -> false
	}
}
