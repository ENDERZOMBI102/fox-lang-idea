package com.enderzombi102.foxlang.idea.psi

import com.enderzombi102.foxlang.idea.FoxFileType
import com.enderzombi102.foxlang.idea.FoxLanguage
import com.intellij.extapi.psi.PsiFileBase
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider

class FoxPsiFile( viewProvider: FileViewProvider ) : PsiFileBase( viewProvider, FoxLanguage ) {
	override fun getFileType(): FileType =
		FoxFileType

	override fun toString(): String =
		"Fox File"
}
