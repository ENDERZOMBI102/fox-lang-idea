package com.enderzombi102.foxlang.idea.psi.impl

import com.enderzombi102.foxlang.idea.psi.FoxNamedElement
import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode

abstract class FoxNamedElementImpl( node: ASTNode ) : ASTWrapperPsiElement( node ), FoxNamedElement
