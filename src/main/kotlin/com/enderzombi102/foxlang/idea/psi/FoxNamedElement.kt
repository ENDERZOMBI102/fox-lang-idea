package com.enderzombi102.foxlang.idea.psi

import com.intellij.psi.PsiNameIdentifierOwner

interface FoxNamedElement : PsiNameIdentifierOwner
