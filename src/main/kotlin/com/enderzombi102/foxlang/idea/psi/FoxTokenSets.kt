package com.enderzombi102.foxlang.idea.psi

import com.enderzombi102.foxlang.idea.psi.FoxElementTypes.*
import com.intellij.psi.tree.IElementType
import com.intellij.psi.tree.TokenSet

object FoxTokenSets {
	private fun tokenSetOf( vararg tokens: IElementType ) =
		TokenSet.create( *tokens )

	val IDENTIFIERS = tokenSetOf( IDENTIFIER )
	val COMMENTS = tokenSetOf( T_LINE_COMMENT, T_BLOCK_COMMENT, T_DOC_COMMENT )

	val KEYWORDS = tokenSetOf(
		T_ABSTRACT, T_AS, T_ASM, T_ATTRIBUTE,
		T_BREAK, T_BY,
		T_CAST, T_CLASS, T_COMPTIME, T_CONST,
		T_DO,
		T_ELSE, T_ENUM, T_EXPORT, T_EXTERN,
		T_FALSE, T_FOR, T_FROM,
		T_IF, T_IMPORT, T_IN, T_INTERFACE, T_IS,
		T_LIKE, T_LOOP,
		T_MACRO, T_MEMORY, T_MUT,
		T_OPERATOR, T_OVERRIDE,
		T_PUB,
		T_REINTERPRET, T_RETURN, T_RUNTIME,
		T_SEALED, T_STATIC, T_STRUCT,
		T_THIS, T_TRUE, T_TYPEALIAS,
		T_WHEN, T_WHILE,
		T_YIELD
	)
	val STRINGS = tokenSetOf( T_STRING )
	val LITERALS = tokenSetOf( T_STRING, T_INTEGER, T_FLOAT )
}
