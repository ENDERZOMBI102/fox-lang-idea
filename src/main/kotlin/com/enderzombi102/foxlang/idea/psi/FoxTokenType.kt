package com.enderzombi102.foxlang.idea.psi

import com.enderzombi102.foxlang.idea.FoxLanguage
import com.intellij.psi.tree.IElementType

class FoxTokenType( debugName: String ) : IElementType( debugName, FoxLanguage )
