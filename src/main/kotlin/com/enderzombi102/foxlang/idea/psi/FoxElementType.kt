package com.enderzombi102.foxlang.idea.psi

import com.enderzombi102.foxlang.idea.FoxLanguage
import com.intellij.psi.tree.IElementType

class FoxElementType( debugName: String ) : IElementType( debugName, FoxLanguage )
