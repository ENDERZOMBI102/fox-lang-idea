@file:JvmName("FoxPsiImplUtil")

package com.enderzombi102.foxlang.idea.psi

import com.intellij.navigation.ItemPresentation
import com.intellij.psi.PsiElement
import javax.swing.Icon


fun getName( element: FoxFuncDecl): String =
	element.identifier.text

fun setName(element: FoxFuncDecl, newName: String? ): PsiElement {
	val keyNode = element.node.findChildByType( FoxElementTypes.FUNC_DECL )
	if ( keyNode != null ) {
		TODO( "Not implemented" )
//		val property = FoxElementTypes.Factory.createProperty( element.project, newName )
//		val newKeyNode = property.getFirstChild().getNode()
//		element.node.replaceChild( keyNode, newKeyNode )
	}
	return element
}

fun getPresentation( element: FoxFuncDecl): ItemPresentation {
	return object : ItemPresentation {
		override fun getPresentableText(): String? =
			element.name

		override fun getLocationString(): String =
			element.containingFile.name

		override fun getIcon( unused: Boolean ): Icon? =
			element.getIcon( 0 )
	}
}

fun getNameIdentifier( element: FoxFuncDecl): PsiElement =
	element.identifier
