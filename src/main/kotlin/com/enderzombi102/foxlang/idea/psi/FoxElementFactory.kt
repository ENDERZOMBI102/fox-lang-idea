package com.enderzombi102.foxlang.idea.psi

import com.enderzombi102.foxlang.idea.FoxFileType
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFileFactory




object FoxElementFactory {
	fun createFunction( proj: Project, name: String ): FoxFuncDecl =
		createFile( proj, name ).firstChild as FoxFuncDecl

	fun createFile( proj: Project, text: String ): FoxPsiFile =
		PsiFileFactory
			.getInstance( proj )
			.createFileFromText( "foxo", FoxFileType, text ) as FoxPsiFile
}
