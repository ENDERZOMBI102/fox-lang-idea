package com.enderzombi102.foxlang.idea.highlight

import com.enderzombi102.foxlang.idea.lexer.FoxLexerAdapter
import com.enderzombi102.foxlang.idea.psi.FoxElementTypes
import com.enderzombi102.foxlang.idea.psi.FoxTokenSets
import com.intellij.lexer.Lexer
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.HighlighterColors
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.psi.TokenType
import com.intellij.psi.tree.IElementType


class FoxSyntaxHighlighter : SyntaxHighlighterBase() {
	override fun getHighlightingLexer(): Lexer =
		FoxLexerAdapter()

	override fun getTokenHighlights( tokenType: IElementType): Array<out TextAttributesKey?> = when ( tokenType ) {
		in FoxTokenSets.KEYWORDS -> KEYWORD_KEYS
		TokenType.BAD_CHARACTER -> BAD_CHAR_KEYS
		FoxElementTypes.T_LINE_COMMENT -> LINE_COMMENT_KEYS
		FoxElementTypes.T_BLOCK_COMMENT -> BLOCK_COMMENT_KEYS
		FoxElementTypes.T_DOC_COMMENT -> DOC_COMMENT_KEYS
		FoxElementTypes.T_STRING -> STRING_KEYS
		FoxElementTypes.T_INTEGER, FoxElementTypes.T_FLOAT -> NUMBER_KEYS
		FoxElementTypes.TYPE -> TYPE_KEYS
		FoxElementTypes.T_DOT -> DOT_KEYS
		else -> EMPTY_KEYS
	}

	companion object {
		private fun key( name: String, key: TextAttributesKey ) =
			createTextAttributesKey( "FOX_$name", key )

		val DOT = key( "DOT", DefaultLanguageHighlighterColors.DOT )
		val KEYWORD = key( "KEYWORD", DefaultLanguageHighlighterColors.KEYWORD )
		val TYPE = key( "TYPE", DefaultLanguageHighlighterColors.CLASS_NAME )
		val STRING = key( "STRING", DefaultLanguageHighlighterColors.STRING )
		val NUMBER = key( "NUMBER", DefaultLanguageHighlighterColors.NUMBER )
		val LINE_COMMENT = key( "LINE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT )
		val BLOCK_COMMENT = key( "BLOCK_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT )
		val DOC_COMMENT = key( "DOC_COMMENT", DefaultLanguageHighlighterColors.DOC_COMMENT )
		val BAD_CHARACTER = key( "BAD_CHARACTER", HighlighterColors.BAD_CHARACTER )

		private val DOT_KEYS = arrayOf( DOT )
		private val KEYWORD_KEYS = arrayOf( KEYWORD )
		private val TYPE_KEYS = arrayOf( TYPE )
		private val STRING_KEYS = arrayOf( STRING )
		private val NUMBER_KEYS = arrayOf( NUMBER )
		private val LINE_COMMENT_KEYS = arrayOf( LINE_COMMENT )
		private val BLOCK_COMMENT_KEYS = arrayOf( BLOCK_COMMENT )
		private val DOC_COMMENT_KEYS = arrayOf( DOC_COMMENT )
		private val BAD_CHAR_KEYS = arrayOf( BAD_CHARACTER )
		private val EMPTY_KEYS = arrayOfNulls<TextAttributesKey>( 0 )
	}
}
