package com.enderzombi102.foxlang.idea.highlight

import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile

class FoxSyntaxHighlighterFactory : SyntaxHighlighterFactory() {
	override fun getSyntaxHighlighter( project: Project?, virtualFile: VirtualFile? ): SyntaxHighlighter =
		FoxSyntaxHighlighter()
}
