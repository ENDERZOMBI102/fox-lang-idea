package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.*
import com.intellij.lang.ASTNode
import com.intellij.lang.folding.CustomFoldingBuilder
import com.intellij.lang.folding.FoldingDescriptor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil

class FoxFoldingBuilder : CustomFoldingBuilder(), DumbAware {
	override fun getLanguagePlaceholderText( node: ASTNode, range: TextRange ): String =
		when ( node.elementType ) {
			FoxElementTypes.T_LBRACE -> " { "
			FoxElementTypes.T_RBRACE -> " }"
			FoxElementTypes.IMPORT -> "..."
			else -> when ( node.psi ) {
				is FoxComment -> "/* ... */"
				is FoxParamList -> "(...)"
				else -> "{...}"
			}
		}

	override fun buildLanguageFoldRegions( descriptors: MutableList<FoldingDescriptor>, root: PsiElement, document: Document, quick: Boolean ) {
		if ( root !is FoxPsiFile )
			return

		val visitor = FoldingVisitor( descriptors )
		PsiTreeUtil.processElements( root ) {
			it.accept( visitor )
			true
		}
	}


	override fun isRegionCollapsedByDefault( node: ASTNode ): Boolean {
		return false
	}

	inner class FoldingVisitor( private val descriptors: MutableList<FoldingDescriptor> ) : FoxVisitor() {
		override fun visitBlock( block: FoxBlock) {
			descriptors += FoldingDescriptor( block.node, block.textRange )
		}
	}
}
