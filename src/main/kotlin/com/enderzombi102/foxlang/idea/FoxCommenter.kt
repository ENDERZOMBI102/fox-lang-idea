package com.enderzombi102.foxlang.idea

import com.intellij.lang.Commenter

class FoxCommenter : Commenter {
	override fun getLineCommentPrefix(): String =
		"//"

	override fun getBlockCommentPrefix(): String =
		"/*"

	override fun getBlockCommentSuffix(): String =
		"*/"

	override fun getCommentedBlockCommentPrefix(): String? =
		null

	override fun getCommentedBlockCommentSuffix(): String? =
		null
}
