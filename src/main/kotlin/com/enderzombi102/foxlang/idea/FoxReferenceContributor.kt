package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxFuncCall
import com.intellij.patterns.PlatformPatterns
import com.intellij.psi.*
import com.intellij.util.ProcessingContext


class FoxReferenceContributor : PsiReferenceContributor() {
	override fun registerReferenceProviders( registrar: PsiReferenceRegistrar ) {
		registrar.registerReferenceProvider( PlatformPatterns.psiElement( FoxFuncCall::class.java ), FoxFunctionReferenceProvider() )
	}

	class FoxFunctionReferenceProvider : PsiReferenceProvider() {
		override fun getReferencesByElement( element: PsiElement, context: ProcessingContext ): Array<PsiReference> =
			arrayOf( FoxPsiReference( element, ( element as FoxFuncCall).identifier.textRange ) )
	}
}
