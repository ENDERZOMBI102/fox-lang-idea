package com.enderzombi102.foxlang.idea

import com.enderzombi102.foxlang.idea.psi.FoxFuncDecl
import com.intellij.navigation.ChooseByNameContributorEx
import com.intellij.navigation.NavigationItem
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.util.Processor
import com.intellij.util.containers.ContainerUtil
import com.intellij.util.indexing.FindSymbolParameters
import com.intellij.util.indexing.IdFilter

class FoxChooseByNameContributor : ChooseByNameContributorEx {
	override fun processNames( processor: Processor<in String?>, scope: GlobalSearchScope, filter: IdFilter? ) {
		val funcNames = ContainerUtil.map( FoxUtils.findFunctions( scope.project!! ), FoxFuncDecl::getName )
		ContainerUtil.process( funcNames, processor )
	}

	override fun processElementsWithName( name: String, processor: Processor<in NavigationItem>, parameters: FindSymbolParameters ) {
		val properties = ContainerUtil.map(
			FoxUtils.findFunctions(
				parameters.project,
				name
			)
		) {
			decl -> decl as NavigationItem
		}
		ContainerUtil.process( properties, processor )
	}
}
