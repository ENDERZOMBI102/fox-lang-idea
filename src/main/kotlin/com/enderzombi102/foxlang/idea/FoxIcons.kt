package com.enderzombi102.foxlang.idea

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon


object FoxIcons {
	val FILE: Icon = IconLoader.getIcon( "/icons/blobfox.png", FoxIcons::class.java )
}
